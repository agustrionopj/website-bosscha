---
toc: true
type: docs
date: 2020-07-19T10:30:00+07:00

linktitle: Astro day in Schools
identifier: astroday

menu:
    media-2:
        parent: Galeri
        weight: 5

weight: 5
---

### SD Pancasila
{{<foldergallery src="pancasila">}}

***
### SD Merdeka
{{<foldergallery src="merdeka">}}