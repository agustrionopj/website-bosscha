---
toc: true
type: docs
date: 2020-07-19T22:43:00+07:00

linktitle: Openhouse Observatorium Bosscha
identifier: openhouse

menu:
    media-2:
        parent: Galeri
        weight: 8

weight: 8
---

{{<foldergallery src="openhouse-bosscha">}}