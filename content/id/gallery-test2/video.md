---
toc: true
type: docs
date: 2020-04-01T00:46:00+07:00

menu:
    media-2:
        parent: Galeri
        name: Video Kegiatan Bosscha
        weight: 9

weight: 9
---

#### Open House Observatorium Bosscha
{{< youtube Naxich818OA>}}

***

#### Indonesia - United Kingdom Partnership in Astronomy
{{< youtube L9KR1iTK658 >}} 

***

#### 90 Tahun Teleskop Refraktor Ganda Zeiss
{{< youtube BsE5HohU3a4 >}}

***

#### Program Pemberdayaan Masyarakat di Sekitar Observatorium Timor - Bagian 1
{{< youtube n45a3BVRmB0 >}}

***

#### Program Pemberdayaan Masyarakat di Sekitar Observatorium Timor - Bagian 2
{{< youtube QcIPhuAThaU >}}

***

#### Teaser Seri Video Pelatihan Astronomi: Astronomi Fundamental

{{< youtube 2ciaRoRPZFo >}}

***

#### 
