---
toc: true
type: docs
date: 2020-07-19T10:30:00+07:00

linktitle: 90 Tahun Teleskop Zeiss
identifier: 90tahunZeiss

menu:
    media-2:
        parent: Galeri
        weight: 4

weight: 4
---

{{<foldergallery src="90zeiss">}}