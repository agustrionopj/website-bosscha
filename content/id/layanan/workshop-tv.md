---
title: Temu Virtual
date: 2021-03-14T00:00:00.000Z
lastmod: 2021-03-15T00:00:00.000Z
draft: false
toc: true
type: docs
menu:
  program-daring:
    parent: Program dan Layanan Daring
    name: Temu Virtual
    weight: 3

weight: 3
---

<style>
.tombol {
  background-color: #417AF5; /* blue */
  border: none;
  color: white;
  padding: 5px 15px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
</style>

Observatorium Bosscha mengadakan serangkaian webinar, gelar wicara (<i>talk show</i>) dan <i>workshop</i> pendidikan terkait astronomi dan topik-topik hangat yang berelasi. Menampilkan pakar-pakar astronomi dan keilmuan yang relevan, program ini dapat diikuti oleh masyarakat umum dan komunitas pecinta astronomi secara gratis. Program bertajuk Temu Virtual ini akan diadakan dua bulan sekali melalui media temu daring Zoom meeting atau Slido dan disiarkan langsung melalui YouTube. 

> **10 April 2021** <br>
**Temu Virtual 1 - Webinar dan Media Briefing Pengamatan Hilal Ramadhan 1442 H** <br>
**Narasumber: 	Hendro Setyanto, Muhammad Yusuf, Premana W. Premadi** <br><br>
Salah satu topik astronomi yang relevan dengan masyarakat adalah kenampakan bulan sabit dan kaitannya dengan penentuan awal bulan pada penanggalan Hijriah. Observatorium Bosscha akan mengadakan webinar bertajuk Pengamatan Hilal dan <i>media conference</i> terkait pengamatan hilal Ramadhan 1442 H. Acara ini dilakukan melalui Zoom <i>meeting</i> dan dapat diikuti oleh masyarakat umum dan wartawan secara gratis.

Tertarik untuk ikut? 

<a href="https://bit.ly/WebinarHilal" target="_blank"><button class="tombol">Daftar di sini</button></a>

<img src="/img/TV01_poster.jpg" width=500></img>