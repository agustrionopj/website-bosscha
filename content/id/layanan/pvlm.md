---
title: Program Pengamatan Virtual Langit Malam 2021
date: 2021-03-14T00:00:00.000Z
lastmod: 2021-03-15T00:00:00.000Z
draft: false
toc: true
type: docs
menu:
  program-daring:
    parent: Program dan Layanan Daring
    name: PVLM 2021
    weight: 2

weight: 2
---

<style>
* {
  box-sizing: border-box;
}

/* Create three equal columns that floats next to each other */
.column {
  float: left;
  width: 33.33%;
  padding: 10px;
  /* text-align: justify;
  text-justify: inter-word; */
  }

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* div.desc {
  padding: 20px;
} */

/* @media screen and (min-width: 601px) {
  p {
    font-size: 16px;
  }
}

@media screen and (max-width: 600px) {
  p {
    font-size: 14px;
  }
} */

.showmore {
  font-size: 0.8em;
}

.showmore .more, .showmore.show .dots {
  display: none
}

.showmore.show .more {
  display: inline
}

.showmore button {
  cursor: pointer;
  display: block;
  margin-top: 0.5em;
  margin-bottom: 1em;
  font-weight: bold;
  background-color: #656565;
  color: white;
  border: none;
  outline: none;
  padding: 0.5em;
}

.tombol {
  background-color: #417AF5; /* blue */
  border: none;
  color: white;
  padding: 5px 15px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
</style>

Selamat datang di halaman Pengamatan Virtual Langit Malam 2021. Mari bergabung bersama kami dalam menjelajahi langit malam, mengamati objek-objek langit melalui salah satu teleskop di Observatorium Bosscha dan mengupas topik-topik astronomi menarik. Episode-episode yang akan datang akan diumumkan di sini dan di media sosial sebelum kegiatan tersebut dilaksanakan. Anda juga dapat menonton kembali episode yang telah dilaksanakan pada kanal YouTube kami. Terima kasih telah menonton! Salam langit gelap, salam langit untuk semua. 

> **9 April 2021** <br>
**Episode 1 - Kabar Terkini Astronomi** <br>
**Narasumber: Premana W. Premadi, Muhammad Yusuf** <br><br>
Astronomi sebagai cabang sains murni mendapat banyak bantuan dari teknologi dalam usaha memahami alam semesta. Meskipun begitu, tidak jarang lompatan teknologi terjadi karena dipicu oleh kebutuhan astronomi. Dua tahun belakangan ini kita melihat ada banyak peristiwa menarik terkait penemuan, peristiwa astronomi, dan usaha eksplorasi astronomi. Mengawali musim baru PVLM, pada episode perdana ini penonton akan diajak melihat capaian astronomi di tahun 2020 dan 2021. Kita kembali menengok apa yang terjadi dengan meredupnya bintang Betelgeuse, arti hadiah Nobel Fisika 2020 bagi Astrofisika, penemuan magnetar, babak baru eksplorasi Planet Mars, dan foto terbaru lubang hitam M87. Narasumber akan mengajak penonton menguak sains dan teknologi apa saja yang terlibat serta rencana dan harapan apa yang ada di depan termasuk peluangnya bagi astronomi Indonesia.

Pendaftaran dimulai pada <font color="red">Kamis, 8 April 2021, 17:00 WIB </font> 

<a href="https://bit.ly/pvlm2021" target="_blank"><button class="tombol">Daftar di sini</button></a>

Bagaimana menyaksikan PVLM 2021?
- Dengan mendaftar Anda akan mendapatkan pemberitahuan melalui email berisikan informasi tautan kegiatan.
- Kegiatan akan dilakukan melalui platform Slido. Dengan meng-klik tautan pertemuan akan akan diarahkan membuka <i>web browser</i> (Google Chrome atau Firefox). Pengguna telepon pintar dapat mengunduh aplikasi melalui <i>Google Play Store</i> atau <i>App Store</i> .
- Seluruh kegiatan dan interaksi akan dilakukan dalam satu halaman web. 


### Arsip PVLM 2021

Pilih **_Watch this video on YouTube_** jika ada pesan kesalahan dalam memutar video.

<div class="row">
  <div class="column">
    <b>Episode 01 - Kabar Terkini Astronomi</b>
  </div>
  <div class="column">
    <b>Episode 02 - TBD</b>
  </div>
  <div class="column">
    <b>Episode 03 - TBD</b>
  </div>
</div>

<div class="row">
  <div class="column">
    {{<youtube >}}
    <p style="font-size: .8em" class="showmore">
    Astronomi sebagai cabang sains murni mendapat banyak bantuan dari teknologi dalam usaha memahami alam semesta. Meskipun begitu, tidak jarang lompatan teknologi terjadi karena dipicu oleh kebutuhan astronomi. <span class="dots">$\ldots$</span><span class="more"> Dua tahun belakangan ini kita melihat ada banyak peristiwa menarik terkait penemuan, peristiwa astronomi, dan usaha eksplorasi astronomi.  Mengawali musim baru PVLM, pada episode perdana ini penonton akan diajak melihat capaian astronomi di tahun 2020 dan 2021. Kita kembali menengok apa yang terjadi dengan meredupnya bintang Betelgeuse, arti hadiah Nobel Fisika 2020 bagi Astrofisika, penemuan magnetar, babak baru eksplorasi Planet Mars, dan foto terbaru lubang hitam M87. Narasumber akan mengajak penonton menguak sains dan teknologi apa saja yang terlibat serta rencana dan harapan apa yang ada di depan termasuk peluangnya bagi astronomi Indonesia. </span>
      <button>Selengkapnya</button>
    <!-- Manusia telah mengamati langit malam sejak zaman dahulu kala. Keteraturan gerak benda-benda langit menginspirasi manusia dari berbagai sudut pandang, antara lain kebudayaan, kepercayaan, mata pencaharian, dan bahkan aktivitas sehari-hari. Mulai dari Bulan, Matahari, dan planet-planet dalam Tata Surya, hingga rasi-rasi bintang yang bergantian menghiasi langit malam. <span class="dots">$\ldots$</span><span class="more">Kita mengenali berbagai rasi bintang yang masih terus kita manfaatkan kemunculannya dan nikmati keindahannya. Rasi-rasi bintang diberi nama dan label mengikuti perpaduan antara kearifan berbagai peradaban kuno dengan aturan baru yang lebih sistematis yang disepakati oleh komunitas astronom dunia. Dalam skala jarak, rasi-rasi bintang tersebar dalam ruang kecil yang di seputar Tata Surya; relatif kecil dibandingkan dengan ukuran Galaksi Bima Sakti. Astronomi modern memanfaatkan rasi-rasi bintang itu sebagai petunjuk arah, sehingga tak jarang penamaan benda langit yang jauh mengikuti nama rasi bintang yang menjadi pengarah ke benda tersebut.  </span>
      <button>Selengkapnya</button> -->
    </p>
  </div>
  <div class="column">
     <!-- {{<youtube iQ7-aChXn7g >}}
    <p style="font-size: .8em" class="showmore">Pada pertemuan ini pemirsa diajak untuk mendengar cerita tentang siklus hidup bintang, seperti apa daerah pembentuk bintang pada umumnya? <span class="dots">$\ldots$</span><span class="more">Tersusun atas apa? Bagaimana bintang lahir di nebula ini? Bagaimana proses kematian serta peran bintang dalam memperkaya unsur kimia di alam semesta? </span>
      <button>Selengkapnya</button>
    </p> -->
  </div>
  <div class="column">
    <!-- {{<youtube 6i4EKFyrMQ0>}}
    <p style="font-size: .8em" class="showmore"> Seperti manusia, masing-masing dari miliaran galaksi di alam semesta mengembangkan sifat uniknya sendiri selama masa hidup yang rumit. <span class="dots">$\ldots$</span><span class="more">Apa saja bentuk dan komponen galaksi, bagaimana galaksi terbentuk dan apa yang dapat galaksi ceritakan tentang alam semesta kita? </span>
      <button>Selengkapnya</button>
    </p> -->
  </div>
</div>

<div class="row">
  <div class="column">
    <p style="font-size: .8em">Narasumber: <br> 1. Premana W. Premadi, Ph.D. <br>  2. Muhammad Yusuf, S.Si.</p>
  </div>
  <div class="column">
    <!-- <p style="font-size: .8em">Narasumber: <br> 1. Dr. Kiki Vierdayanti <br>  2. Muhammad Yusuf, S.Si.</p> -->
  </div>
  <div class="column">
    <!-- <p style="font-size: .8em">Narasumber: <br> 1. Premana W. Premadi, Ph.D. <br>  2. Muhammad Yusuf, S.Si.</p> -->
  </div>
</div>

### [Arsip PVLM 2020]({{< ref "/layanan/pvlm-2020.md">}})

<script> 
  document.querySelectorAll(".showmore").forEach(function (p) {
   p.querySelector("button").addEventListener("click", function () {
    p.classList.toggle("show");
    this.textContent = p.classList.contains("show") ? "Persingkat" : "Selengkapnya";
   });
 }); 
</script>