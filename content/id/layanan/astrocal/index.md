---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

#title: "Newsletter"
#linktitle: "Newsletter"
summary:
date: 2020-01-30T12:33:42+07:00
lastmod: 2020-01-30T12:33:42+07:00
draft: false  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: docs  # Do not modify.

weight: 1

# Add menu entry to sidebar.
# - Substitute `example` with the name of your course/documentation folder.
# - name: Declare this menu item as a parent with ID `name`.
# - parent: Reference a parent ID if this page is a child.
# - weight: Position of link in menu.
menu:
  media:
    parent: Media
    name: Kalender Astronomi
    # parent: Fasilitas
    weight: 4

---

<!-- <h1 align="center">Kalender Astronomi 2020</h1> -->

## <i class="far fa-calendar-alt"></i> Kalender Astronomi

---
- Tab <button type="button" class="btn btn-primary">Bulan</button> dan <button type="button" class="btn btn-primary">Minggu</button> berturut-turut menampilkan tampilan kalender bulanan dan mingguan. Tab <button type="button" class="btn btn-primary">Agenda</button> menampilkan daftar peristiwa astronomi pada bulan yang ditinjau. 
- Klik/tap nama peristiwa untuk menampilkan detil peristiwa.

<iframe src="/html/kalender/index.html" width="800" height="1000" frameborder="0" style="border:0" allowfullscreen></iframe>