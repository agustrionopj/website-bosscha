---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Rilis Media"
linktitle: "Rilis Media"
summary:
date: 2021-03-30T12:33:42+07:00
lastmod: 2021-04-01T00:33:42+07:00
draft: false  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: docs  # Do not modify.
weight: 1

# Add menu entry to sidebar.
# - Substitute `example` with the name of your course/documentation folder.
# - name: Declare this menu item as a parent with ID `name`.
# - parent: Reference a parent ID if this page is a child.
# - weight: Position of link in menu.
menu:
  media:
    weight: 1
    parent: Media
    name: Rilis Media
    # parent: Fasilitas
    
---

* {{% staticref "files/press-rilis-OB-tutup-1Januari2021.pdf" "newtab" %}} Press Release Bosscha Tutup terkait COVID-19 Tahun 2021 {{% /staticref %}}
* {{% staticref "files/ObsBosscha-Press-Release-Dzulhijjah-2020.pdf" "newtab" %}} Press Release Hilal Dzulhijjah 1441 H/2020 M {{% /staticref %}}
* {{% staticref "files/ObsBosscha-Press-Release-Syawal-2020.pdf" "newtab" %}} Press Release Hilal Syawal 1441 H/2020 M {{% /staticref %}}
* {{% staticref "files/ObsBosscha-Press-Release-Ramadan-2020.pdf" "newtab" %}} Press Release Hilal Ramadan 1441 H/2020 M {{% /staticref %}}
* {{% staticref "files/press-release-OB-tutup-13Maret2020.pdf" "newtab" %}} Press Release Observatorium Bosscha Tutup terkait COVID-19 {{% /staticref %}} 
* {{% staticref "files/press-release-muharram-2019.pdf" "newtab" %}} Press Release Hilal Muharram 1441 H/2019 M {{% /staticref %}}
* {{% staticref "files/press-release-syawal-2019.pdf" "newtab" %}} Press Release Hilal Syawal 1440 H/2019 M {{% /staticref %}}
* {{% staticref "files/press-release-ramadhan-2019.pdf" "newtab" %}} Press Release Hilal Ramadhan 1440 H/2019 M {{% /staticref %}}
* {{% staticref "files/press-release-ramadhan-2018.pdf" "newtab" %}} Press Release Hilal Ramadhan 1439 H/2018 M {{% /staticref %}}
* {{% staticref "files/press-release-hilal-syawal-2018.pdf" "newtab" %}} Press Release Hilal Syawal 1439 H/2018 M {{% /staticref %}}
* {{% staticref "files/press-release-muharram-2018.pdf" "newtab" %}} Press Release Muharram 1440 H/2018 M {{% /staticref %}}
* {{% staticref "files/press-release-safar-2018.pdf" "newtab" %}} Press Release Safar 1440 H/2018 M {{% /staticref %}}
* {{% staticref "files/Press-release-GBT-Jan-2018-rev.pdf" "newtab" %}} Press Release Gerhana Bulan Total 31 Januari 2018 {{% /staticref %}}
* {{% staticref "files/Press-release-GBTJuli-2018.pdf" "newtab" %}} Press Release Gerhana Bulan Total 28 Juli 2018 dan Oposisi Mars {{% /staticref %}}
* {{% staticref "files/pressrelease-90thn-zeiss.pdf" "newtab" %}} Press Release 90 Tahun Teleskop Refraktor Ganda Zeiss 1 Desember 2018 {{% /staticref %}}
* {{% staticref "files/Press-release-syawal-2017.pdf" "newtab" %}} Press Release Hilal Syawal 1438 H/2017 M {{% /staticref %}}
* {{% staticref "files/Press-release-syawal-2016.pdf" "newtab" %}} Press Release Hilal Syawal 1437 H/2016 M {{% /staticref %}}


